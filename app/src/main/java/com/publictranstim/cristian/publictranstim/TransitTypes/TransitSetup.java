package com.publictranstim.cristian.publictranstim.TransitTypes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;

/**
 * Created by Cristian on 04.07.2015.
 */
public class TransitSetup {

    Context context;
    private ArrayList selectedTransit;
    private int index;

    public TransitSetup(Context context) {

        selectedTransit = new ArrayList<String>();

        this.context = context;
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        //CHECK FOR BUSSes
        index = 0;

        if (sharedPreferences.getBoolean("enable_busA3", false) == true)    selectedTransit.add(index++, TransitTypes.BUSA3);
        if (sharedPreferences.getBoolean("enable_busA13", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA13);
        if (sharedPreferences.getBoolean("enable_busA13b", false) == true)  selectedTransit.add(index++, TransitTypes.BUSA13b);
        if (sharedPreferences.getBoolean("enable_busA21", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA21);
        if (sharedPreferences.getBoolean("enable_busA22", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA22);
        if (sharedPreferences.getBoolean("enable_busA28", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA28);
        if (sharedPreferences.getBoolean("enable_busA29", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA29);
        if (sharedPreferences.getBoolean("enable_busA32", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA32);
        if (sharedPreferences.getBoolean("enable_busA33", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA33);
        if (sharedPreferences.getBoolean("enable_busA33b", false) == true)  selectedTransit.add(index++, TransitTypes.BUSA33b);
        if (sharedPreferences.getBoolean("enable_busA40", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA40);
        if (sharedPreferences.getBoolean("enable_busA46", false) == true)   selectedTransit.add(index++, TransitTypes.BUSA46);

        //CHECK FOR TRAMs
        if (sharedPreferences.getBoolean("enable_tram1", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM1);
        if (sharedPreferences.getBoolean("enable_tram2", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM2);
        if (sharedPreferences.getBoolean("enable_tram4", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM4);
        if (sharedPreferences.getBoolean("enable_tram5", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM5);
        if (sharedPreferences.getBoolean("enable_tram6", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM6);
        if (sharedPreferences.getBoolean("enable_tram6b", false) == true)   selectedTransit.add(index++, TransitTypes.TRAM6b);
        if (sharedPreferences.getBoolean("enable_tram7", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM7);
        if (sharedPreferences.getBoolean("enable_tram7b", false) == true)   selectedTransit.add(index++, TransitTypes.TRAM7b);
        if (sharedPreferences.getBoolean("enable_tram8", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM8);
        if (sharedPreferences.getBoolean("enable_tram9", false) == true)    selectedTransit.add(index++, TransitTypes.TRAM9);
        if (sharedPreferences.getBoolean("enable_tram10", false) == true)   selectedTransit.add(index++, TransitTypes.TRAM10);

        //CHECK FOR TROLLEYs
        if (sharedPreferences.getBoolean("enable_trolley11", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY11);
        if (sharedPreferences.getBoolean("enable_trolley14", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY14);
        if (sharedPreferences.getBoolean("enable_trolley15", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY15);
        if (sharedPreferences.getBoolean("enable_trolley16", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY16);
        if (sharedPreferences.getBoolean("enable_trolley17", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY17);
        if (sharedPreferences.getBoolean("enable_trolley18", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY18);
        if (sharedPreferences.getBoolean("enable_trolley19", false) == true)    selectedTransit.add(index++, TransitTypes.TROLLEY19);

        //CHECK FOR EXPRESSes
        if (sharedPreferences.getBoolean("enable_express1", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS1);
        if (sharedPreferences.getBoolean("enable_express2", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS2);
        if (sharedPreferences.getBoolean("enable_express3", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS3);
        if (sharedPreferences.getBoolean("enable_express4", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS4);
        if (sharedPreferences.getBoolean("enable_express4b", false) == true)    selectedTransit.add(index++, TransitTypes.EXPRESS4b);
        if (sharedPreferences.getBoolean("enable_express5", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS5);
        if (sharedPreferences.getBoolean("enable_express6", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS6);
        if (sharedPreferences.getBoolean("enable_express7", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS7);
        if (sharedPreferences.getBoolean("enable_express7b", false) == true)    selectedTransit.add(index++, TransitTypes.EXPRESS7b);
        if (sharedPreferences.getBoolean("enable_express8", false) == true)     selectedTransit.add(index++, TransitTypes.EXPRESS8);
        if (sharedPreferences.getBoolean("enable_express33", false) == true)    selectedTransit.add(index++, TransitTypes.EXPRESS33);

        //CHECK FOR METROPOLITANs
        if (sharedPreferences.getBoolean("enable_metropolitan30", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN30);
        if (sharedPreferences.getBoolean("enable_metropolitan35", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN35);
        if (sharedPreferences.getBoolean("enable_metropolitan36", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN36);
        if (sharedPreferences.getBoolean("enable_metropolitan43", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN43);
        if (sharedPreferences.getBoolean("enable_metropolitan44", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN44);
        if (sharedPreferences.getBoolean("enable_metropolitan45", false) == true)   selectedTransit.add(index++, TransitTypes.METROPOLITAN45);

    }

    public int getSize(){
        return selectedTransit.size();
    }

    public String getLineId(int i){
        return String.valueOf(selectedTransit.get(i));
    }
}
