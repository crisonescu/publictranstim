package com.publictranstim.cristian.publictranstim;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.publictranstim.cristian.publictranstim.AboutScreen.AboutFragment;
import com.publictranstim.cristian.publictranstim.MapScreen.MapsFragment;
import com.publictranstim.cristian.publictranstim.SettingsScreen.SettingsFragment;

import java.io.File;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends ActionBarActivity

        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    /**
     * Stores the current selected fragment
     */
    Fragment objFragment = null;
    int pressed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);

        rateapp(); // show this at first startup

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void rateapp(){

        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //String mapTypePref = sharedPreferences.getString("pref_key_TypeValues", "");


        SharedPreferences sharedPreferences = getSharedPreferences("version", 0);
        int savedVersionCode = sharedPreferences.getInt("VersionCode", 0);

        int appVershionCode = 0;

        try {
            appVershionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

        } catch (PackageManager.NameNotFoundException nnfe) {
            Log.w("test", "$ Exception caz of appVershionCode : " + nnfe);
        }

        if(savedVersionCode == appVershionCode){
            Log.d("test", "$$ savedVersionCode == appVershionCode");
        }else{
            Log.d("test", "$$ savedVersionCode != appVershionCode");

            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
            sharedPreferencesEditor.putInt("VersionCode", appVershionCode);
            sharedPreferencesEditor.commit();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Rate me maybe? :3");
            alertDialogBuilder.setMessage("Would you like to rate my app? pls. ");

            alertDialogBuilder.setPositiveButton("Bine boss!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Log.d("test", "rate");
                    Toast.makeText(getApplicationContext(), "App still in development...", Toast.LENGTH_SHORT).show();
                }
            });

            alertDialogBuilder.setNeutralButton("Alta data..", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d("test", "ignore");
                    Toast.makeText(getApplicationContext(), "mkay.. :'(", Toast.LENGTH_SHORT).show();

                }
            });

            alertDialogBuilder.show();
        }

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        //Reset backbutton press counter
        pressed = 0;

        switch (position){
            case 0:
                objFragment = new MapsFragment();
                break;
            case 1:
                objFragment = new SettingsFragment();
                break;
            case 2:
                objFragment = new AboutFragment();
                break;
        }

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, objFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        pressed++;
        if(pressed == 1){
            Toast.makeText(this, getString(R.string.message_quit), Toast.LENGTH_SHORT).show();
        }else if(pressed == 2){
            finish();
            System.exit(0);
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

/*
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(!objFragment.getClass().equals(new SettingsFragment().getClass())) {
            if (id == R.id.action_settings) {
                objFragment = new SettingsFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, objFragment)
                        .commit();
                Log.e("NOT CHANGED", "CANNOT CHANGE DAT SHET");
            }
        }

        return super.onOptionsItemSelected(item);
    }
    */

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
}
