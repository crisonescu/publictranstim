package com.publictranstim.cristian.publictranstim.SplashScreen;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;

import com.publictranstim.cristian.publictranstim.IntroScreen.IntroActivity;
import com.publictranstim.cristian.publictranstim.MainActivity;
import com.publictranstim.cristian.publictranstim.R;

/**
 * Created by Cristian on 19.03.2015.
 */
public class SplashScreen extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                SharedPreferences sharedPreferences = getSharedPreferences("version", 0);
                int savedVersionCode = sharedPreferences.getInt("VersionCode", 0);

                int appVershionCode = 0;

                try {
                    appVershionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

                } catch (PackageManager.NameNotFoundException nnfe) {
                    Log.w("test", "$ Exception caz of appVershionCode : " + nnfe);
                }

                if(savedVersionCode == appVershionCode){
                    Log.d("test", "$$ savedVersionCode == appVershionCode");

                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }else {
                    Log.d("test", "$$ savedVersionCode != appVershionCode");

                    Intent i = new Intent(SplashScreen.this, IntroActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
