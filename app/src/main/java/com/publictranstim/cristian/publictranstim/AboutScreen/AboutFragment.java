package com.publictranstim.cristian.publictranstim.AboutScreen;

        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

        import com.publictranstim.cristian.publictranstim.R;

/**
 * Created by Cristian on 16.03.2015.
 */
public class AboutFragment extends Fragment {
    View rootview;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_about, container, false);
        return rootview;
    }
}
