package com.publictranstim.cristian.publictranstim.SettingsScreen;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.preference.PreferenceFragment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.publictranstim.cristian.publictranstim.R;

/**
 * Created by Cristian on 16.03.2015.
 */
public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity activity = getActivity();
        if (isAdded() && activity != null) {

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            EditTextPreference nickname = (EditTextPreference) findPreference("pref_key_username");
            if(nickname != null) {
                nickname.setSummary(nickname.getText());

                nickname.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        EditTextPreference etp = (EditTextPreference) preference;
                        String newHostValue = newValue.toString();
                        etp.setSummary(newHostValue);
                        return true;
                    }
                });
            }
            EditTextPreference totalDistance = (EditTextPreference) findPreference("pref_totalDistance");
            if(totalDistance != null) {
                totalDistance.setSummary(totalDistance.getText());
            }
        }
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}

