package com.publictranstim.cristian.publictranstim.MapScreen;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.publictranstim.cristian.publictranstim.DatabaseConfig.DbConfig;
import com.publictranstim.cristian.publictranstim.Logic.TransitServer;
import com.publictranstim.cristian.publictranstim.R;
import com.publictranstim.cristian.publictranstim.TransitTypes.TransitSetup;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cristian on 16.03.2015.
 */
public class MapsFragment extends Fragment {

    private static View rootview;

    private GoogleMap mMap;
    private Marker userMarker;
    private double totalDistanceFromStartPoint = 0;
    private LatLng prevUserLocation;
    private TextView userTotalDistance;
    private TextView nextStationDistance;
    private TextView arrivingTimeNextStation;
    String arrivingTime;
    private String tempLineID;
    private String distanceToNextStation;

    private ArrayList<LatLng> line3;
    private ArrayList<LatLng> line13;

    private List<Integer> stationTimes;
    private List<Location> stationLocations;

    private Cursor cursor;
    private DbConfig db;

    private Thread t;

    private final LatLng LOCATION_TIMISOARA = new LatLng(45.7536135, 21.2263422);

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        stationTimes = new ArrayList<>();
        stationLocations = new ArrayList<Location>();

        if (rootview != null) {
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if (parent != null)
                parent.removeView(rootview);
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_maps, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }


        //region SETUP MAP
        if (getActivity() != null && isAdded()) {

            setUpMapIfNeeded();

            final TransitSetup transitList = new TransitSetup(this.getActivity());
            if ((mMap != null) && transitList != null) {
                db = new DbConfig(getActivity());
                //setupDatabase(transitSetup.getLineIds(this.getActivity()));
                int size = transitList.getSize();

                class RefreshMap implements Runnable {
                    TransitSetup transitListTemp;
                    int index;

                    RefreshMap(TransitSetup transitList, int i) {
                        transitListTemp = transitList;
                        index = i;
                    }

                    public void run() {
                        mMap.clear();
                        setupDatabase(transitListTemp.getLineId(index));
                        Toast.makeText(getActivity(), "Reactualizare harta...", Toast.LENGTH_LONG).show();
                    }
                }

                for (int i = 0; i < size; i++) {

                    if (transitList.getLineId(i) != null) {
                        final Handler handler = new Handler();
                        Timer timer = new Timer();
                        final int index = i;
                        TimerTask doAsynchronousTask = new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(new RefreshMap(transitList, index));
                            }
                        };
                        timer.schedule(doAsynchronousTask, 0, 60000);
                        //setupDatabase(transitList.getLineId(i));

                    }
                }
            }
        }
        //endregion

        return rootview;
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */

    //
    private void setUpMapIfNeeded() {

        int temp = 0;

        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {

                /**
                 * The following line displays the current user location according to user preferences it is enabled or disabled.
                 */
                mMap.setMyLocationEnabled(sharedPreferences.getBoolean("pref_key_myLocation", true));

                /**
                 * This sets the position of user in realtime
                 */

                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location arg0) {
                        if (userMarker != null) {
                            userMarker.remove();
                        }

                        String path = new String(new File(Environment.getExternalStorageDirectory() + "/PublicTransTim/userProfilePhoto.jpg").getAbsolutePath());

                        try {
                            // TODO Auto-generated method stub
                            userMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude()))
                                    .title(String.valueOf(sharedPreferences.getString("pref_key_username", "Cristian")))
                                    .snippet(getString(R.string.maps_snippet_currentposition))
                                    //.icon(BitmapDescriptorFactory.fromResource(R.drawable.sample_user_pic)));
                                    .icon(BitmapDescriptorFactory.fromPath(path)));
                        }catch (Exception e){
                            // TODO Auto-generated method stub
                            userMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude()))
                                    .title(String.valueOf(sharedPreferences.getString("pref_key_username", "Cristian")))
                                    .snippet(getString(R.string.maps_snippet_currentposition))
                                    //.icon(BitmapDescriptorFactory.fromResource(R.drawable.sample_user_pic)));
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.sample_user_pic)));
                        }

                        if (prevUserLocation != null) {
                            totalDistanceFromStartPoint = totalDistanceFromStartPoint + getDistance(new LatLng(arg0.getLatitude(), arg0.getLongitude()), prevUserLocation);

                            //Toast.makeText(getActivity(), "Total Distance:" + String.valueOf(totalDistanceFromStartPoint), Toast.LENGTH_SHORT).show();

                            userTotalDistance = (TextView) getActivity().findViewById(R.id.userTotalDistance);
                            nextStationDistance = (TextView) getActivity().findViewById(R.id.nextStationDistance);
                            arrivingTimeNextStation = (TextView) getActivity().findViewById(R.id.time);
                            if (totalDistanceFromStartPoint != 0 && userTotalDistance != null) {
                                userTotalDistance.setText("Dist. tot.: " + String.valueOf((int) totalDistanceFromStartPoint) + " m");
                                try {
                                    nextStationDistance.setText(String.valueOf(getDistanceToTheNextStation(new LatLng(arg0.getLatitude(), arg0.getLongitude()))) + "m pana la statie");
                                    arrivingTimeNextStation.setText("Sosire: " + arrivingTime);
                                }catch (Exception e){}
                                /**
                                 * adds distance to total distance from preferences
                                 * TODO: FIX!!
                                 */
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String totalDistance = sharedPreferences.getString("pref_totalDistance", "0");
                                int tempTotalDistance = (int) totalDistanceFromStartPoint + Integer.parseInt(totalDistance);
                                editor.putString("pref_totalDistance", String.valueOf(tempTotalDistance));
                                editor.commit();


                            }
                        }
                        prevUserLocation = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                    }
                });

                setUpMap();

                temp++;
            }
        }
        //mMap.clear();
    }
    //

    private void setUpMap() {

        //mMap.addMarker(new MarkerOptions().position(new LatLng(45.7536135, 21.2263422)).title("Marker"));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());

        /**
         * This is going to display map type according to user preferences
         */
        String mapTypePref = sharedPreferences.getString("pref_key_TypeValues", "");
        if ("MAP_TYPE_NORMAL".equals(mapTypePref)) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if ("MAP_TYPE_HYBRID".equals(mapTypePref)) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if ("MAP_TYPE_SATELLITE".equals(mapTypePref)) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else if ("MAP_TYPE_TERRAIN".equals(mapTypePref)) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        } else if ("MAP_TYPE_NONE".equals(mapTypePref)) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        }


        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_TIMISOARA, 13);
        mMap.animateCamera(update);

        mMap.getUiSettings().setRotateGesturesEnabled(false);

        //showToast(getString(R.string.maps_toast_welcome) + String.valueOf(sharedPreferences.getString("pref_key_username", "Cristian")));
    }

    private double getDistance(LatLng p, LatLng q) {
        double c = Math.PI / 180;
        // Google (gives randomly wrong results in Android!)
        //return google.maps.geometry.spherical.computeDistanceBetween(p,q);
        // Chord
        //return 9019995.5222 * Math.sqrt((1-Math.cos(c*(p.lat()-q.lat())))
        //   + (1-Math.cos(c*(p.lng()-q.lng()))) * Math.cos(c*p.lat()) * Math.cos(c*q.lat()));
        // Taylor for chord
        return 111318.845 * Math.sqrt(Math.pow(p.latitude - q.latitude, 2)
                + Math.pow(p.longitude - q.longitude, 2) * Math.cos(c * p.latitude) * Math.cos(c * q.latitude));
    }

    private int getDistanceToTheNextStation(LatLng userCurrentLocation){

        //Convert LatLng to Location
        Location currentLocation = new Location("userCurrentLocation");
        currentLocation.setLatitude(userCurrentLocation.latitude);
        currentLocation.setLongitude(userCurrentLocation.longitude);
        currentLocation.setTime(new Date().getTime()); //Set time as current Date

        double tempDistance = currentLocation.distanceTo(stationLocations.get(0));
        arrivingTime = "0 min";

        for(int i = 0; i < stationLocations.size(); i++) {
            double distanceInMeters = currentLocation.distanceTo(stationLocations.get(i));
            if(tempDistance>distanceInMeters){
                tempDistance = distanceInMeters;
                arrivingTime = stationLocations.get(i).getProvider();
            }
        }
        return (int) tempDistance;
    }


    /**
     * need to update this for more threads in paralel
     *
     * @param tableName
     */
    public void setupDatabase(String tableName) {
        new GetDataTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, tableName);
    }

    private class GetDataTask extends AsyncTask<String, Integer, String> {

        // Runs in UI before background thread is called
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Do something like display a progress bar
        }


        @Override
        protected String doInBackground(String... params) {

            cursor = db.getDatabaseEntries(params[0]); // you would not typically call this on the main thread

            return null;
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (cursor.getCount() > 0) {
                cursor.moveToPosition(-1);
                while (cursor.moveToNext()) {
                    try {

                        String sosire1 = "";
                        String sosire2 = "";

                        TransitServer transitServer = new TransitServer();

                        String transitServerResponse = "";
                        try {
                            //first param is traseu id
                            //second param is station id
                            transitServerResponse = transitServer.execute(new String[]{cursor.getString(5), cursor.getString(4)}).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(), transitServerResponse, Toast.LENGTH_SHORT).show();


                        double latitude = Double.parseDouble(cursor.getString(2));
                        double longitude = Double.parseDouble(cursor.getString(3));

                        Pattern pattern = Pattern.compile("Sosire1: (.*?)<br>");
                        Matcher matcher = pattern.matcher(transitServerResponse);
                        while (matcher.find()) {
                            sosire1 = "Sosire 1: " + matcher.group(1);

                            String _tmp = matcher.group(1);

                            //Convert LatLng to Location
                            Location location = new Location(_tmp);
                            location.setLatitude(latitude);
                            location.setLongitude(longitude);
                            location.setTime(new Date().getTime()); //Set time as current Date
                            stationLocations.add(location);

                            if (_tmp.contains(">>")) {
                                stationTimes.add(0);
                            } else {
                                _tmp = _tmp.replaceAll("[^0-9]", "");
                                int _tmpInt = Integer.parseInt(_tmp);
                                stationTimes.add(_tmpInt);
                            }

                        }

                        Pattern pattern2 = Pattern.compile("Sosire2: (.*?)</b>");
                        Matcher matcher2 = pattern2.matcher(transitServerResponse);
                        while (matcher2.find()) {
                            sosire2 = "Sosire 2: " + matcher2.group(1);
                        }

                        try {
                            if (stationTimes.get(stationTimes.size() - 2) > stationTimes.get(stationTimes.size() - 1)) {
                                mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                                        .title(cursor.getString(0))
                                        .snippet(cursor.getString(1) + " " + sosire1 + " " + sosire2)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.transitmarker)));
                            }
                        } catch (Exception eGetTransitPosition) {

                        }

                        if (sosire1.contains(" 1 min") || sosire2.contains(" 1 min") ||
                                sosire1.contains(" 2 min") || sosire2.contains(" 2 min") ||
                                sosire1.contains(" 3 min") || sosire2.contains(" 3 min") ||
                                sosire1.contains(" 4 min") || sosire2.contains(" 4 min") ||
                                sosire1.contains(" 5 min") || sosire2.contains(" 5 min") ||
                                sosire1.contains(" 6 min") || sosire2.contains(" 6 min") ||
                                sosire1.contains(" 7 min") || sosire2.contains(" 7 min") ||
                                sosire1.contains(" 8 min") || sosire2.contains(" 8 min") ||
                                sosire1.contains(" 9 min") || sosire2.contains(" 9 min")) {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                                    .title(cursor.getString(0))
                                    .snippet(cursor.getString(1) + " " + sosire1 + " " + sosire2)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.tx1)));
                        } else if (sosire1.contains(" 10 min") || sosire2.contains(" 10 min") ||
                                sosire1.contains(" 11 min") || sosire2.contains(" 11 min") ||
                                sosire1.contains(" 12 min") || sosire2.contains(" 12 min") ||
                                sosire1.contains(" 13 min") || sosire2.contains(" 13 min") ||
                                sosire1.contains(" 14 min") || sosire2.contains(" 14 min") ||
                                sosire1.contains(" 15 min") || sosire2.contains(" 15 min")) {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                                    .title(cursor.getString(0))
                                    .snippet(cursor.getString(1) + " " + sosire1 + " " + sosire2)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.tx2)));
                        } else {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                                    .title(cursor.getString(0))
                                    .snippet(cursor.getString(1) + " " + sosire1 + " " + sosire2)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.tx3)));
                        }



                        //Log.e(cursor.getString(1), "FSN: " + cursor.getString(0) + " LN: " + cursor.getString(1) + " Lat: " + cursor.getString(2) + " Long: " + cursor.getString(3));
                        //Toast.makeText(getActivity(), "FSN: " + cursor.getString(0) + " LN: " + cursor.getString(1) + " Lat: " + cursor.getString(2) + " Long: " + cursor.getString(3), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.d("tst", e.toString());
                    }
                }
            }
            // Do things like hide the progress bar or change a TextView
        }

    }
}
