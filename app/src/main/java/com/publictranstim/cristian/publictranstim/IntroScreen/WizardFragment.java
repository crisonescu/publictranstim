package com.publictranstim.cristian.publictranstim.IntroScreen;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.publictranstim.cristian.publictranstim.R;

import java.io.File;


/**
 * Created by Cristian on 06.05.2015.
 */
@SuppressLint("ValidFragment")
public class WizardFragment extends Fragment {

    int wizard_page_position;
    View rootview;

    EditText name;
    ImageView profileImage;


    private ImageView image;
    private String[] mapTypes;
    private Spinner spinner;
    private TypedArray imgs;
    private LoginButton loginButton;

    private CallbackManager mCallbackManager;
    private FacebookCallback<LoginResult> mCallback;

    public WizardFragment(int position) {
        this.wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        int layout_id = R.layout.page1;
        switch (wizard_page_position) {
            case 0:
                layout_id = R.layout.page1;
                break;

            case 1:
                layout_id = R.layout.page2;
                break;

            case 2:
                layout_id = R.layout.page3;
                break;

            case 3:
                layout_id = R.layout.page4;
                break;

            case 4:
                layout_id = R.layout.page5;
                break;
        }

        rootview = inflater.inflate(layout_id, container, false);

        switch (wizard_page_position) {
            case 1:
                IntroPage2Fragment();
                break;

            case 2:
                IntroPage3Fragment();
                break;

            case 3:
                IntroPage4Fragment();
                break;
        }

        return rootview;
    }


    public void IntroPage2Fragment() {
        name = (EditText) rootview.findViewById(R.id.name);
        profileImage = (ImageView) rootview.findViewById(R.id.profilePhoto);
        final String[] userId = new String[1];

        loginButton = (LoginButton) rootview.findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);

        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile profile = Profile.getCurrentProfile();
                name.setText(profile.getName());


                userId[0] = loginResult.getAccessToken().getUserId();
                String profileImgUrl = "https://graph.facebook.com/" + userId[0] + "/picture?type=large";


//                Glide.with(getActivity().getApplicationContext())
//                        .load(profileImgUrl)
//                        .into(profileImage);

                //saveFacebookProfilePicture(getActivity(), getFacebookProfilePicture(profileImgUrl), "FBProfilePicture.png");

                // Execute DownloadImage AsyncTask
                new DownloadImage().execute(profileImgUrl);

                if (name.requestFocus()) {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }

                try {
                    if (Profile.getCurrentProfile().getName() != null) {
                        name.setText(Profile.getCurrentProfile().getName());
                    }
                }catch (Exception e){}

            }

            @Override
            public void onCancel() {
                name.setHint("Introdu numele...");
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                name.setHint("Introdu numele...");
            }
        });
        try {
            if (Profile.getCurrentProfile().getName() != null) {
                name.setText(Profile.getCurrentProfile().getName());
            }
        }catch (Exception e){}
        if(name != null) {
            name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        //<<<<<< UPDATE NICKNAME >>>>>>>
                        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                        name = (EditText) rootview.findViewById(R.id.name);
                        sharedPreferencesEditor.putString("pref_key_username", name.getText().toString());
                        sharedPreferencesEditor.putString("pref_key_fbUserId", userId[0]);
                        sharedPreferencesEditor.commit();
                    }
                    return false;
                }
            });
        }

//        getFacebookProfile();

    }

    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... URL) {

            String imageURL = URL[0];

            try {
                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/PublicTransTim");

                if (!direct.exists()) {
                    direct.mkdirs();
                }

                DownloadManager mgr = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

                Uri downloadUri = Uri.parse(imageURL);
                DownloadManager.Request request = new DownloadManager.Request(
                        downloadUri);

                request.setAllowedNetworkTypes(
                        DownloadManager.Request.NETWORK_WIFI
                                | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false).setTitle("Retrieving photo...")
                        .setDescription("Synchronizing profile photo")
                        .setDestinationInExternalPublicDir("/PublicTransTim", "userProfilePhoto.jpg");

                mgr.enqueue(request);
            }catch (Exception e) {

            }
                return "true";

        }

        @Override
        protected void onPostExecute(String result) {
            // Set the bitmap into ImageView
            //image.setImageBitmap(result);
            // Close progressdialog
            //mProgressDialog.dismiss();
        }
    }
/*
    public void saveFacebookProfilePicture(Context context, Bitmap bitmap, String picName){
        OutputStream output;

        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/PublicTransTim");
        dir.mkdirs();

        File file = new File(dir, picName);

        try {
            output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.flush();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
    /*
    public static Bitmap getFacebookProfilePicture(String userID){
        String imageURL;
        Bitmap bitmap = null;
        Log.d("getfbpic", "Loading Picture");
        imageURL = "http://graph.facebook.com/"+userID+"/picture?type=small";
        try {
            bitmap = BitmapFactory.decodeStream((InputStream)new URL(imageURL).getContent());
        } catch (Exception e) {
            Log.d("TAG", "Loading Picture FAILED");
            e.printStackTrace();
        }
        return bitmap;
    }
*/
    public void IntroPage3Fragment() {

        mapTypes = getResources().getStringArray(R.array.Types);
        imgs = getResources().obtainTypedArray(R.array.TypeStyles);

        image = (ImageView) rootview.findViewById(R.id.pag3img1);
        spinner = (Spinner) rootview.findViewById(R.id.type_spinner);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mapTypes);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                image.setImageResource(imgs.getResourceId(spinner.getSelectedItemPosition(), -1));

                final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

                switch (spinner.getSelectedItemPosition()) {
                    case 0:
                        sharedPreferencesEditor.putString("pref_key_TypeValues", "MAP_TYPE_NORMAL");
                        break;

                    case 1:
                        sharedPreferencesEditor.putString("pref_key_TypeValues", "MAP_TYPE_HYBRID");
                        break;

                    case 2:
                        sharedPreferencesEditor.putString("pref_key_TypeValues", "MAP_TYPE_SATELLITE");
                        break;

                    case 3:
                        sharedPreferencesEditor.putString("pref_key_TypeValues", "MAP_TYPE_TERRAIN");
                        break;

                    case 4:
                        sharedPreferencesEditor.putString("pref_key_TypeValues", "MAP_TYPE_NONE");
                        break;
                }
                sharedPreferencesEditor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    public void IntroPage4Fragment() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void getFacebookProfile() {

        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            name.setText(profile.getName());

            if (name != null) {
                name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                            //<<<<<< UPDATE NICKNAME >>>>>>>
                            sharedPreferencesEditor.putString("pref_key_username", name.getText().toString());
                            sharedPreferencesEditor.commit();
                        }
                        return false;
                    }
                });
            }
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

            sharedPreferencesEditor.putString("pref_key_fbUserId", String.valueOf(profile.getId())).apply();
            sharedPreferencesEditor.commit();
        }else{
            if(name != null) {
                name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            //<<<<<< UPDATE NICKNAME >>>>>>>
                            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                            name = (EditText) rootview.findViewById(R.id.name);
                            sharedPreferencesEditor.putString("pref_key_username", name.getText().toString());
                            sharedPreferencesEditor.commit();
                        }
                        return false;
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        Profile profile = Profile.getCurrentProfile();
//        name.setText(message(profile));
    }

    private String message(Profile profile) {
        StringBuilder stringBuffer = new StringBuilder();
        if (profile != null) {
            stringBuffer.append(profile.getName());
        }
        return stringBuffer.toString();
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
