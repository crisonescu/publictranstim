package com.publictranstim.cristian.publictranstim.TransitTypes;

/**
 * Created by Cristian on 05.07.2015.
 */
public class TransitTypes {

    //TODO:     enable_busAXXXX
    public static final String BUSA3     = "1207";
    public static final String BUSA13    = "1066";
    public static final String BUSA13b   = "2546";
    public static final String BUSA21    = "1146";
    public static final String BUSA22    = "2566";
    public static final String BUSA28    = "1226";
    public static final String BUSA29    = "2626";
    public static final String BUSA32    = "1546";
    public static final String BUSA33    = "1046";
    public static final String BUSA33b   = "2466";
    public static final String BUSA40    = "886";
    public static final String BUSA46    = "1406";

    //TODO:     enable_tramXXXX
    public static final String TRAM1     = "1106";
    public static final String TRAM2     = "1126";
    public static final String TRAM4     = "1266";
    public static final String TRAM5     = "1553";
    public static final String TRAM6     = "2686";
    public static final String TRAM6b    = "2706";
    public static final String TRAM7     = "1556";
    public static final String TRAM7b    = "1557";
    public static final String TRAM8     = "1558";
    public static final String TRAM9     = "2406";
    public static final String TRAM10    = "2726";

    //TODO:     enable_trolleyXXXX
    public static final String TROLLEY11    = "990";
    public static final String TROLLEY14    = "1006";
    public static final String TROLLEY15    = "989";
    public static final String TROLLEY16    = "1206";
    public static final String TROLLEY17    = "1086";
    public static final String TROLLEY18    = "1166";
    public static final String TROLLEY19    = "1186";

    //TODO:     enable_expressXXXX
    public static final String EXPRESS1     = "1550";
    public static final String EXPRESS2     = "1551";
    public static final String EXPRESS3     = "1552";
    public static final String EXPRESS4     = "1926";
    public static final String EXPRESS4b    = "2486";
    public static final String EXPRESS5     = "1526";
    public static final String EXPRESS6     = "1928";
    public static final String EXPRESS7     = "2026";
    public static final String EXPRESS7b    = "1326";
    public static final String EXPRESS8     = "1547";
    public static final String EXPRESS33    = "2586";

    //TODO:     enable_metropolitanXXXX
    public static final String METROPOLITAN30   = "1746";
    public static final String METROPOLITAN35   = "1986";
    public static final String METROPOLITAN36   = "2006";
    public static final String METROPOLITAN43   = "2646";
    public static final String METROPOLITAN44   = "2506";
    public static final String METROPOLITAN45   = "2606";
}
